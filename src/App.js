import React, {useState} from "react";
import {ThemeProvider} from '@material-ui/core/styles';
import {Button, Grid, Paper} from "@material-ui/core";
import useMuiTheme from "./useMuiTheme";
import ButtonWithError from "./components/ButtonWithError";

function App() {
    const theme = useMuiTheme();
    const [error, setError] = useState(false)
    const toggleError = () => {
        setError(!error)
    }

    return (
        <ThemeProvider theme={theme}>
            <Grid container style={{minHeight: '100vh'}} alignItems='center' justify='center'>
                <Paper>
                    <Grid item container alignItems='center' justify='center' wrap='wrap' spacing={2}>
                        <Grid item container direction='row' alignItems='center' justify='center' wrap='wrap' xs={12}
                              sm={6}>
                            <ButtonWithError onClick={toggleError} error={error} variant="contained"  color="primary" fullWidth='true'>Primary</ButtonWithError>
                        </Grid>
                        <Grid item container direction='row' alignItems='center' justify='center' wrap='wrap' xs={12}
                              sm={6}>
                            <ButtonWithError onClick={toggleError} error={error} variant="contained" color="secondary" fullWidth='true'>Secondary</ButtonWithError>
                        </Grid>
                        <Grid item container direction='row' alignItems='center' justify='center' wrap='wrap' xs={12}
                              sm={6}>
                            <ButtonWithError onClick={toggleError} error={error} variant="contained" color="secondary" fullWidth='true'>Third</ButtonWithError>
                        </Grid>
                        <Grid item container direction='row' alignItems='center' justify='center' wrap='wrap' xs={12}
                              sm={6}>
                            <ButtonWithError onClick={toggleError} error={error} variant="contained" color="primary" fullWidth='true'>Fourth</ButtonWithError>
                        </Grid>
                    </Grid>
                </Paper>
            </Grid>
        </ThemeProvider>
    );
}

export default App;
