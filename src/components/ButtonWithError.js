import React from "react";
import { makeStyles, createStyles } from '@material-ui/core/styles';
import {Button} from "@material-ui/core";

const useStyles = makeStyles((theme) => (
    {
        root: error => ({
            backgroundColor: error && theme.palette.error.main
        })
    }
));

export default function ButtonWithError({error = false, ...rest}) {

    const classes = useStyles(error);
    return <Button {...rest} className={classes.root} />;
}

