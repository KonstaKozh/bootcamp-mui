import {createMuiTheme} from "@material-ui/core";

const useMuiTheme = () => {
    return createMuiTheme({
        root: {
            height: '100%',
        },
        palette: {
            primary: {
                main: '#0F4780'
            },
            secondary: {
                main: '##6B64FF'
            }
        },
        breakpoints: {
            values: {
                sm: 700,
            },
        },
        overrides: {
            MuiButton: {
                root: {
                    padding: "10px 16px",
                },
                label: {
                    fontSize: '16px',
                    fontWeight: "bold"
                },
                containedSecondary: {
                    '&:hover': {
                        background: "transparent"
                    },

                }
            },
            MuiPaper: {
                root: {
                    width: '400px',
                    padding: '20px',
                },
                rounded: {
                    borderRadius: '8px'
                }
            },
        },
    })
};

export default useMuiTheme;
